<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $country_name
 * @property string $country_name_en
 * @property string $created_at
 *
 * @property User[] $users
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['country_name','country_name_en'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_name' => 'Country Name',
            'country_name_en' => 'Country Name EN',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['country_id' => 'id']);
    }

    public static function getCitiesByCountry($country_id = false){

    	if(!$country_id)
    		return $country_id;
		else
	        $cities = City::find()->select(['id as city_id' ,'city_name','city_name_en'])->where(['country_id' => $country_id])->asArray()->all();

		if(!empty($cities))
			return $cities;
		else
			return false;
    }

}
