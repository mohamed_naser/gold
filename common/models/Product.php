<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $special_for
 * @property string $product_name
 * @property string $price
 * @property string $price_note
 * @property integer $price_type
 * @property integer $city
 * @property integer $qty
 * @property integer $caliber
 * @property integer $weight
 * @property integer $user_type
 * @property string $mobile
 * @property string $default_image_source
 * @property string $default_image_name
 * @property string $notes
 * @property string $created_at
 * @property integer $country_id
 * @property integer $product_type
 * @property integer $manufacturer_id
 * @property integer $brand_id
 * @property integer $activate
 * @property integer $Gold_type
 * @property integer $is_paid_adds
 *
 * @property City $city0
 * @property UserType $userType
 * @property User $user
 * @property ProductImages[] $productImages
 * @property UserFavoriteProduct[] $userFavoriteProducts
 * @property User[] $users
 */
class Product extends \yii\db\ActiveRecord
{
    public static $new_gold = 0 ;
    public static $used_gold = 1 ;

    public static $paid_ads = 1 ;
    public static $free_ads = 0 ;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'special_for', 'price_type', 'city', 'qty', 'caliber', 'user_type', 'country_id', 'product_type', 'manufacturer_id', 'brand_id', 'activate', 'Gold_type', 'is_paid_adds'], 'integer'],
            [['city', 'user_type'], 'required'],
            [['created_at','weight'], 'safe'],
            [['product_name', 'price'], 'string', 'max' => 45],
            [['price_note', 'default_image_source', 'default_image_name'], 'string', 'max' => 150],
            [['mobile'], 'string', 'max' => 60],
            [['notes'], 'string', 'max' => 300],
            [['city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city' => 'id']],
            [['user_type'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['user_type' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'special_for' => 'Special For',
            'product_name' => 'Product Name',
            'price' => 'Price',
            'price_note' => 'Price Note',
            'price_type' => 'Price Type',
            'city' => 'City',
            'qty' => 'Qty',
            'caliber' => 'Caliber',
            'weight' => 'Weight',
            'user_type' => 'User Type',
            'mobile' => 'Mobile',
            'default_image_source' => 'Default Image Source',
            'default_image_name' => 'Default Image Name',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'country_id' => 'Country ID',
            'product_type' => 'Product Type',
            'manufacturer_id' => 'Manufacturer ID',
            'brand_id' => 'Brand ID',
            'activate' => 'Activate',
            'Gold_type' => 'Gold Type',
            'is_paid_adds' => 'Is Paid Adds',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['id' => 'user_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFavoriteProducts()
    {
        return $this->hasMany(UserFavoriteProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_favorite_product', ['product_id' => 'id']);
    }
}
