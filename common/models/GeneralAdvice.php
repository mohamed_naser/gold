<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "generalAdvice".
 *
 * @property integer $advice_id
 * @property string $title
 * @property string $description
 */
class GeneralAdvice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'generalAdvice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'advice_id' => 'Advice ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
