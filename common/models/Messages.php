<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $message_id
 * @property integer $sender_id
 * @property integer $reciver_id
 * @property string $message
 * @property string $thread_id
 * @property string $created_at
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'reciver_id'], 'integer'],
            [['created_at'], 'safe'],
            [['message'], 'string', 'max' => 300],
            [['thread_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id' => 'Message ID',
            'sender_id' => 'Sender ID',
            'reciver_id' => 'Reciver ID',
            'message' => 'Message',
            'thread_id' => 'Thread ID',
            'created_at' => 'Created At',
        ];
    }

    public static function generateThreadID($sender_id , $reciver_id ){

		$users = [$sender_id , $reciver_id];
	    sort($users);

	    $threadId = md5(implode(",", $users));
	    return $threadId;

    }

    public static function getPartnerProfile ($user_id , $thrad_id){

    	$users = Messages::find()->select(['sender_id' , 'reciver_id'])->where(['thread_id' => $thrad_id])->asArray()->one();

    	if($users['sender_id'] == $user_id){
			$partner_id = $users['reciver_id'];
	    }elseif ($users['reciver_id'] == $user_id){
		    $partner_id = $users['sender_id'];
	    }

	    $partner_profile = User::find()->where(['id' => $partner_id])->select(['email','user_image','status','longitude','latitude','user_type','country_id','city_id','username','phone_number','user_show_name'])->asArray()->one();

    	return $partner_profile;
    }
}
