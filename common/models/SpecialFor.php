<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "special_for".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_en
 * @property string $created_at
 */
class SpecialFor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special_for';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['name','name_en'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_en' => 'Name EN',
            'created_at' => 'Created At',
        ];
    }
}
