<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $notification_id
 * @property integer $user_id
 * @property integer $notification_type
 * @property string $notification_message
 * @property integer $read
 * @property string $created_at
 */
class Notifications extends \yii\db\ActiveRecord
{
	const MESSAGE_NOTIFICATION_TYPE = 1;
	CONST NEW_PRODUCT               = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'notification_type', 'read'], 'integer'],
            [['created_at'], 'safe'],
            [['notification_message'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => 'Notification ID',
            'user_id' => 'User ID',
            'notification_type' => 'Notification Type',
            'notification_message' => 'Notification Message',
            'read' => 'Read',
            'created_at' => 'Created At',
        ];
    }
}
