<?php

use yii\db\Migration;

class m170709_140520_change_image_id_to_be_autoincrement extends Migration
{
    public function safeUp()
    {
    	$this->execute("ALTER TABLE `product_images` 
								CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;
						");

    }

    public function safeDown()
    {
        echo "m170709_140520_change_image_id_to_be_autoincrement cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170709_140520_change_image_id_to_be_autoincrement cannot be reverted.\n";

        return false;
    }
    */
}
