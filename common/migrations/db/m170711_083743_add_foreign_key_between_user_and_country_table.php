<?php

use yii\db\Migration;

class m170711_083743_add_foreign_key_between_user_and_country_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
							ALTER TABLE `user` 
							CHANGE COLUMN `country_id` `country_id` INT(10) UNSIGNED NULL DEFAULT NULL ,
							CHANGE COLUMN `city_id` `city_id` INT(10) UNSIGNED NULL DEFAULT NULL ;


							ALTER TABLE `user` 
								ADD CONSTRAINT `user_country`
								  FOREIGN KEY (country_id)
								  REFERENCES `country` (id)
								  ON DELETE NO ACTION
								  ON UPDATE NO ACTION;
								  
                            ALTER TABLE `user` 
								ADD CONSTRAINT `user_city`
								  FOREIGN KEY (city_id)
								  REFERENCES `city` (id)
								  ON DELETE NO ACTION
								  ON UPDATE NO ACTION; 
								  
                            ALTER TABLE `user` 
								ADD CONSTRAINT `user_type`
								  FOREIGN KEY (user_type)
								  REFERENCES `user_type` (id)
								  ON DELETE NO ACTION
								  ON UPDATE NO ACTION; 

						");
    }

    public function safeDown()
    {
        echo "m170711_083743_add_foreign_key_between_user_and_country_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170711_083743_add_foreign_key_between_user_and_country_table cannot be reverted.\n";

        return false;
    }
    */
}
