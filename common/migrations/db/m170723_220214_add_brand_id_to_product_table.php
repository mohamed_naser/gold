<?php

use yii\db\Migration;

class m170723_220214_add_brand_id_to_product_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
		ALTER TABLE `product` 
					ADD COLUMN `brand_id` INT(10) NULL AFTER `manufacturer_id`;
		");
    }

    public function safeDown()
    {
        echo "m170723_220214_add_brand_id_to_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170723_220214_add_brand_id_to_product_table cannot be reverted.\n";

        return false;
    }
    */
}
