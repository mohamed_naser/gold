<?php

use yii\db\Migration;

/**
 * Handles the creation of table `brands`.
 */
class m170723_215111_create_brands_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

    	$this->execute("
    	CREATE TABLE `brands` (
					  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
					  `brand_name` VARCHAR(45) NULL,
					  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
					  PRIMARY KEY (`id`));

    	
    	");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('brands');
    }
}
