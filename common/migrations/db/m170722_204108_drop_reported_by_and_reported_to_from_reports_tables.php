<?php

use yii\db\Migration;

class m170722_204108_drop_reported_by_and_reported_to_from_reports_tables extends Migration
{
    public function safeUp()
    {
    	$this->execute("    	
				      
				      ALTER TABLE `report` 
									DROP FOREIGN KEY `reported_for`,
									DROP FOREIGN KEY `reported_by`;
									ALTER TABLE `report` 
									DROP COLUMN `reported_to`,
									DROP COLUMN `reported_by`,
									DROP INDEX `reported_for_idx` ,
									DROP INDEX `reported_by_idx` ;


    	");

    }

    public function safeDown()
    {
        echo "m170722_204108_drop_reported_by_and_reported_to_from_reports_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170722_204108_drop_reported_by_and_reported_to_from_reports_tables cannot be reverted.\n";

        return false;
    }
    */
}
