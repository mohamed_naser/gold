<?php

use yii\db\Migration;

/**
 * Handles the creation of table `special_for`.
 */
class m170722_150534_create_special_for_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("
        
        CREATE TABLE IF NOT EXISTS `special_for` (
								  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
								  `name` VARCHAR(45) NULL,
								  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
								  PRIMARY KEY (`id`))
								ENGINE = InnoDB;
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('special_for');
    }
}
