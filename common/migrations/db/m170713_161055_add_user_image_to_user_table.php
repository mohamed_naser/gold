<?php

use yii\db\Migration;

class m170713_161055_add_user_image_to_user_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
		ALTER TABLE `user` 
				ADD COLUMN `user_image` VARCHAR(45) NULL AFTER `user_show_name`;

		");
    }

    public function safeDown()
    {
        echo "m170713_161055_add_user_image_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170713_161055_add_user_image_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
