<?php

use yii\db\Migration;

class m170712_124119_add_default_inage_location_to_product_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
		ALTER TABLE `product` 
			ADD COLUMN `default_image_source` VARCHAR(150) NULL AFTER `mobile`,
			ADD COLUMN `default_image_name` VARCHAR(150) NULL AFTER `default_image_source`;
		
		");
    }

    public function safeDown()
    {
        echo "m170712_124119_add_default_inage_location_to_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170712_124119_add_default_inage_location_to_product_table cannot be reverted.\n";

        return false;
    }
    */
}
