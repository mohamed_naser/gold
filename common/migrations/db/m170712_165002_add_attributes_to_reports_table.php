<?php

use yii\db\Migration;

class m170712_165002_add_attributes_to_reports_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
		
		ALTER TABLE `report` 
					ADD COLUMN `reported_by` INT(11) NULL AFTER `created_at`,
					ADD COLUMN `reported_to` INT(11) NULL AFTER `reported_by`,
					ADD INDEX `reported_by_idx` (`reported_by` ASC),
					ADD INDEX `reported_for_idx` (`reported_to` ASC);
					ALTER TABLE  `report` 
					ADD CONSTRAINT `reported_by`
					  FOREIGN KEY (`reported_by`)
					  REFERENCES  `user` (`id`)
					  ON DELETE NO ACTION
					  ON UPDATE NO ACTION,
					ADD CONSTRAINT `reported_for`
					  FOREIGN KEY (`reported_to`)
					  REFERENCES  `user` (`id`)
					  ON DELETE NO ACTION
					  ON UPDATE NO ACTION;
		
		");
    }

    public function safeDown()
    {
        echo "m170712_165002_add_attributes_to_reports_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170712_165002_add_attributes_to_reports_table cannot be reverted.\n";

        return false;
    }
    */
}
