<?php

use yii\db\Migration;

class m170711_082957_add_additional_user_attributes extends Migration
{
    public function safeUp()
    {
		$this->execute("
						ALTER TABLE `user` 
								ADD COLUMN `country_id` INT(10) NULL AFTER `status`,
								ADD COLUMN `city_id` INT(10) NULL AFTER `country_id`,
								ADD COLUMN `phone_number` VARCHAR(45) NULL AFTER `city_id`,
								ADD COLUMN `map` VARCHAR(45) NULL AFTER `phone_number`,
								CHANGE COLUMN `updated_at` `updated_at` INT(11) NULL DEFAULT NULL AFTER `map`;
								
						ALTER TABLE `user` 
								ADD COLUMN `user_show_name` VARCHAR(150) NULL AFTER `map`,
								ADD COLUMN `user_type` INT(10) NULL AFTER `user_show_name`;
			  
		
		");
    }

    public function safeDown()
    {
        echo "m170711_082957_add_additional_user_attributes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170711_082957_add_additional_user_attributes cannot be reverted.\n";

        return false;
    }
    */
}
