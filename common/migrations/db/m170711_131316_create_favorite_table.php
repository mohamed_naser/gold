<?php

use yii\db\Migration;

/**
 * Handles the creation of table `favorite`.
 */
class m170711_131316_create_favorite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
     $this->execute("
	     CREATE TABLE IF NOT EXISTS  `user_favorite_product` (
			  `user_id` INT(11) NOT NULL,
			  `product_id` INT(10) UNSIGNED NOT NULL,
			  `created_at` VARCHAR(45) NULL,
			  PRIMARY KEY (`user_id`, `product_id`),
			  INDEX `fk_user_has_product_product1_idx` (`product_id` ASC),
			  INDEX `fk_user_has_product_user1_idx` (`user_id` ASC),
			  CONSTRAINT `fk_user_has_product_user1`
			    FOREIGN KEY (`user_id`)
			    REFERENCES  `user` (`id`)
			    ON DELETE NO ACTION
			    ON UPDATE NO ACTION,
			  CONSTRAINT `fk_user_has_product_product1`
			    FOREIGN KEY (`product_id`)
			    REFERENCES  `product` (`id`)
			    ON DELETE NO ACTION
			    ON UPDATE NO ACTION)
			ENGINE = InnoDB
			DEFAULT CHARACTER SET = utf8
			COLLATE = utf8_unicode_ci;
     ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('favorite');
    }
}
