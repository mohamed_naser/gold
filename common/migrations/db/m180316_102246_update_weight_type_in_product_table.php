<?php

use yii\db\Migration;

class m180316_102246_update_weight_type_in_product_table extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `product` 
                                    CHANGE COLUMN `weight` `weight` DOUBLE NULL DEFAULT NULL ;
                                    ");
    }

    public function safeDown()
    {
        echo "m180316_102246_update_weight_type_in_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180316_102246_update_weight_type_in_product_table cannot be reverted.\n";

        return false;
    }
    */
}
