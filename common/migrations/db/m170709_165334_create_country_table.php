<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m170709_165334_create_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("
	            CREATE TABLE IF NOT EXISTS `country` (
														  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
														  `country_name` VARCHAR(150) NULL,
														  `created_at` TIMESTAMP NULL,
														  PRIMARY KEY (`id`))
															ENGINE = InnoDB;
        
        ");

        $this->execute("
						        ALTER TABLE `city` 
										CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL ,
										ADD COLUMN `country_id` INT(8) NULL AFTER `city_name`;

       					 ");

        $this->execute("
						       ALTER TABLE `city` 
											CHANGE COLUMN `country_id` `country_id` INT(10) NULL ;
											ALTER TABLE `city` 
											ADD CONSTRAINT `coutry_have_cities`
											  FOREIGN KEY (country_id)
											  REFERENCES `country` (id)
											  ON DELETE NO ACTION
											  ON UPDATE NO ACTION;
	
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
    }
}
