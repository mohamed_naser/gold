<?php

use yii\db\Migration;

class m170707_105124_initial_gold_project extends Migration
{
    public function safeUp()
    {
		$this->execute("
		-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gold
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema gold
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gold` DEFAULT CHARACTER SET utf8 ;
USE `gold` ;

-- -----------------------------------------------------
-- Table `gold`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gold`.`city` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_name` VARCHAR(60) NULL,
  `created_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gold`.`user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gold`.`user_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gold`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gold`.`product` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `special_for` INT NULL,
  `product_name` VARCHAR(45) NULL,
  `price` VARCHAR(45) NULL,
  `city` INT UNSIGNED NOT NULL,
  `qty` SMALLINT NULL,
  `caliber` SMALLINT NULL,
  `weight` MEDIUMINT NULL,
  `user_type` INT UNSIGNED NOT NULL,
  `mobile` VARCHAR(60) NULL,
  `notes` VARCHAR(300) NULL,
  `created_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_product_city_idx` (`city` ASC),
  INDEX `fk_product_user_type1_idx` (`user_type` ASC),
  CONSTRAINT `fk_product_city`
    FOREIGN KEY (`city`)
    REFERENCES `gold`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_user_type1`
    FOREIGN KEY (`user_type`)
    REFERENCES `gold`.`user_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gold`.`product_images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gold`.`product_images` (
  `id` INT UNSIGNED NOT NULL,
  `source` VARCHAR(150) NULL,
  `image_name` VARCHAR(150) NULL,
  `product_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_product_images_product1_idx` (`product_id` ASC),
  CONSTRAINT `fk_product_images_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `gold`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gold`.`manufacturer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gold`.`manufacturer` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `manufacturer_city_name` VARCHAR(60) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

		
		");
    }

    public function safeDown()
    {
        echo "m170707_105124_initial_gold_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170707_105124_initial_gold_project cannot be reverted.\n";

        return false;
    }
    */
}
