<?php

use yii\db\Migration;

class m170713_150838_add_country_id_to_product_table extends Migration
{
    public function safeUp()
    {
		$this->execute("
		ALTER TABLE `product` 
				ADD COLUMN `country_id` INT(11) NULL AFTER `created_at`;
				
				ALTER TABLE `product` 
				ADD CONSTRAINT `fk_product_country`
				  FOREIGN KEY (country_id)
				  REFERENCES `country` (id)
				  ON DELETE NO ACTION
				  ON UPDATE NO ACTION;
		
		");
    }

    public function safeDown()
    {
        echo "m170713_150838_add_country_id_to_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170713_150838_add_country_id_to_product_table cannot be reverted.\n";

        return false;
    }
    */
}
