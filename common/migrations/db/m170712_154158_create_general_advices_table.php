<?php

use yii\db\Migration;

/**
 * Handles the creation of table `general_advices`.
 */
class m170712_154158_create_general_advices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $this->execute("
       
       CREATE TABLE IF NOT EXISTS `generalAdvice` (
								  `advice_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
								  `title` VARCHAR(100) NULL,
								  `description` VARCHAR(200) NULL,
								  PRIMARY KEY (`advice_id`))
								ENGINE = InnoDB;

       ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('general_advices');
    }
}
