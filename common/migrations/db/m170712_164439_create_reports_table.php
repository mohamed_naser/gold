<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reports`.
 */
class m170712_164439_create_reports_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("
        CREATE TABLE IF NOT EXISTS `report` (
						  `report_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
						  `title` VARCHAR(45) NULL,
						  `description` VARCHAR(250) NULL,
						  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
						  PRIMARY KEY (`report_id`))
						ENGINE = InnoDB;
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reports');
    }
}
