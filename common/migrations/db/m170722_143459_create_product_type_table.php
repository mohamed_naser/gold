<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_type`.
 */
class m170722_143459_create_product_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	     $this->execute("
	   		   			  CREATE TABLE IF NOT EXISTS `product_type` (
														  `product_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
														  `name` VARCHAR(45) NULL,
														  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
														  PRIMARY KEY (`product_type_id`))
														ENGINE = InnoDB;
						  ALTER TABLE `product` 
												ADD COLUMN `product_type` INT(10) NULL AFTER `country_id`,
												ADD COLUMN `manufacturer_id` INT(10) NULL AFTER `product_type`;

	    
     ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_type');
    }
}
