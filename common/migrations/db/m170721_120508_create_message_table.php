<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m170721_120508_create_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $this->execute("
							   CREATE TABLE `messages` (
													`message_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
													`sender_id` INT(10) NULL,
													`reciver_id` INT(10) NULL,
													`message` VARCHAR(300) NULL,
													`thread_id` VARCHAR(45) NULL,
													`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
													PRIMARY KEY (`message_id`));
       
       ");

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('message');
    }
}
