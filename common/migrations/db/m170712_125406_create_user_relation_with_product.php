<?php

use yii\db\Migration;

class m170712_125406_create_user_relation_with_product extends Migration
{
    public function safeUp()
    {
		$this->execute("
		
								ALTER TABLE `product` 
										ADD COLUMN `user_id` INT(11) NULL AFTER `id`,
										ADD INDEX `user_own_product_idx` (`user_id` ASC);
										ALTER TABLE `product` 
										ADD CONSTRAINT `user_own_product`
										  FOREIGN KEY (`user_id`)
										  REFERENCES `user` (`id`)
										  ON DELETE NO ACTION
										  ON UPDATE NO ACTION;

		");
    }

    public function safeDown()
    {
        echo "m170712_125406_create_user_relation_with_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170712_125406_create_user_relation_with_product cannot be reverted.\n";

        return false;
    }
    */
}
