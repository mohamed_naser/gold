<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">


    <p>
        <?php echo Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
	        [
		        'attribute' => 'user_id',
		        'value' => function($model) {
			        if(empty($model->user_id))
				        return 'N/A user';

			        if(empty(\common\models\User::findOne(['id' => $model->user_id])->username))
				        return 'N/A User ';

			        return \common\models\User::findOne(['id' => $model->user_id])->username;
		        }
	        ],
	        [
		        'attribute' => 'special_for',
		        'value' => function($model) {
			        if(empty($model->special_for))
				        return 'N/A Special ';

			        if(empty(\common\models\SpecialFor::findOne(['id' => $model->special_for])->name))
				        return 'N/A Special ';

			        return \common\models\SpecialFor::findOne(['id' => $model->special_for])->name;
		        }
	        ],
            'product_name',
            'price',
	        [
		        'attribute' => 'country_id',
		        'value' => function($model) {
			        if(empty($model->country_id))
				        return 'N/A Country ';

			        if(empty(\common\models\Country::findOne(['id' => $model->country_id])->country_name))
				        return 'N/A Country ';

			        return \common\models\Country::findOne(['id' => $model->country_id])->country_name;
		        }
	        ],
	        [
		        'attribute' => 'city',
		        'value' => function($model) {
			        if(empty($model->city))
				        return 'N/A city ';

			        if(empty(\common\models\City::findOne(['id' => $model->city])->city_name))
				        return 'N/A city ';

			        return \common\models\City::findOne(['id' => $model->city])->city_name;
		        }
	        ],
             'qty',
             'caliber',
             'weight',
            // 'user_type',
             'mobile',
            // 'default_image_source',
            // 'default_image_name',
             'notes',
            // 'created_at',
            // 'country_id',
            // 'product_type',
            // 'manufacturer_id',
            // 'brand_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
