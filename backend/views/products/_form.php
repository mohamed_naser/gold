<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $default = empty($model->user_id)?: $model->user_id;

    echo $form->field($model, 'user_id')
              ->dropDownList(ArrayHelper::map(\common\models\User::find()->select(['id','username'])->all(),'id','username')
               ,['value' => $default ]);
    ?>


	<?php
	$default = empty($model->special_for)?: $model->special_for;

	echo $form->field($model, 'special_for')
	          ->dropDownList(ArrayHelper::map(\common\models\SpecialFor::find()->select(['id','name'])->all(),'id','name')
		          ,['value' => $default ]);
	?>

    <?php echo $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

	<?php
	$default = empty($model->country_id)?: $model->country_id;

	echo $form->field($model, 'country_id')
	          ->dropDownList(ArrayHelper::map(\common\models\Country::find()->select(['id','country_name'])->all(),'id','country_name')
		          ,['value' => $default ]);
	?>

	<?php
	$default = empty($model->city)?: $model->city;

	echo $form->field($model, 'city')
	          ->dropDownList(ArrayHelper::map(\common\models\City::find()->select(['id','city_name'])->all(),'id','city_name')
		          ,['value' => $default ]);
	?>


    <?php echo $form->field($model, 'qty')->textInput() ?>

    <?php echo $form->field($model, 'caliber')->textInput() ?>

    <?php echo $form->field($model, 'weight')->textInput() ?>
    
    <?php echo $form->field($model, 'is_paid_adds')->dropDownList([\common\models\Product::$free_ads => 'unpaid ads ' , \common\models\Product::$paid_ads => 'paid ads ']) ?>

	<?php
	$default = empty($model->user_type)?: $model->user_type;

	echo $form->field($model, 'user_type')
	          ->dropDownList(ArrayHelper::map(\common\models\UserType::find()->select(['id','user_type'])->all(),'id','user_type')
		          ,['value' => $default ]);
	?>


    <?php echo $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

<!--    --><?php //echo $form->field($model, 'default_image_source')->textInput(['maxlength' => true]) ?>
<!---->
<!--    --><?php //echo $form->field($model, 'default_image_name')->textInput(['maxlength' => true]) ?>


	<?php
	$default = empty($model->product_type)?: $model->product_type;

	echo $form->field($model, 'product_type')
	          ->dropDownList(ArrayHelper::map(\common\models\ProductType::find()->select(['product_type_id','name'])->all(),'product_type_id','name')
		          ,['value' => $default ]);
	?>

	<?php
	$default = empty($model->manufacturer_id)?: $model->manufacturer_id;

	echo $form->field($model, 'manufacturer_id')
	          ->dropDownList(ArrayHelper::map(\common\models\Manufacturer::find()->select(['id','manufacturer_city_name'])->all(),'id','manufacturer_city_name')
		          ,['value' => $default ]);
	?>

	<?php
	$default = empty($model->brand_id)?: $model->brand_id;

	echo $form->field($model, 'brand_id')
	          ->dropDownList(ArrayHelper::map(\common\models\Brands::find()->select(['id','brand_name'])->all(),'id','brand_name')
		          ,['value' => $default ]);
	?>

	<?php echo $form->field($model, 'notes')->textarea() ?>


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
