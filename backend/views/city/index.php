<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">


    <p>
        <?php echo Html::a('Create City', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'city_name',
            'city_name_en',
	        [
		        'attribute' => 'country_id',
		        'value'=> function($data)
		        {
			        $value =  \common\models\Country::find()->select('country_name')->where(['id' => $data->country_id])->asArray()->one();
			        if(!empty($value))
                        return $value['country_name'];
			        else
			            return 'country Deleted ';
		        },
		        'format' => 'raw'
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
