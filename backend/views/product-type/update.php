<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductType */

$this->title = 'Update Product Type: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-type-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
