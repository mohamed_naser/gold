<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SpecialFor */

$this->title = 'Create Special For';
$this->params['breadcrumbs'][] = ['label' => 'Special Fors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="special-for-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
