<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SpecialFor */

$this->title = 'Update Special For: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Special Fors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="special-for-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
