<?php
namespace frontend\modules\user\models;

use cheatsheet\Time;
use common\commands\SendEmailCommand;
use common\models\User;
use common\models\UserToken;
use frontend\modules\user\Module;
use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{
    /**
     * @var
     */
    public $username;
    public $language;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password;
	/**
	 * @var
	 */
	public $country_id = null;
	/**
	 * @var
	 */
	public $city_id = null;
	/**
	 * @var
	 */
	public $longitude = null;

	public $latitude = null;
	/**
	 * @var
	 */
	public $phone_number = null;
	/**
	 * @var
	 */
	public $user_type = null;
	/**
	 * @var
	 */
	public $user_show_name = null;

	public $user_image = null;

	public $shop_name = null;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
	        [['phone_number', 'longitude' , 'latitude'], 'string', 'max' => 45],
	        [['user_show_name','shop_name'], 'string', 'max' => 150],
	        [['country_id', 'city_id' , 'user_type'], 'integer'],
            ['username', 'required'],
            ['username', 'unique',
                'targetClass'=>'\common\models\User',
                'message' => Yii::t('frontend', 'This username has already been taken.')
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['language', 'string', 'max' => 5],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass'=> '\common\models\User',
                'message' => Yii::t('frontend', 'This email address has already been taken.')
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'=>Yii::t('frontend', 'Username'),
            'email'=>Yii::t('frontend', 'E-mail'),
            'password'=>Yii::t('frontend', 'Password'),
        ];
    }
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $shouldBeActivated = $this->shouldBeActivated();
            $user = new User();

            $user->username         = $this->username;
            $user->email            = $this->email;
            $user->status           = $shouldBeActivated ? User::STATUS_NOT_ACTIVE : User::STATUS_ACTIVE;
            $user->setPassword($this->password);
            $user->country_id       = $this->country_id;
            $user->city_id          = $this->city_id;
            $user->longitude        = $this->longitude;
            $user->latitude         = $this->latitude;
            $user->phone_number     = $this->phone_number;
            $user->user_type        = $this->user_type;
            $user->user_show_name   = $this->user_show_name;
            $user->shop_name        = $this->shop_name;
            $user->user_image       = $this->user_image;
            $user->language       = $this->language;

            if(!$user->save()) {
                throw new Exception("User couldn't be  saved");
            };
            $user->afterSignup();
            if ($shouldBeActivated) {
                $token = UserToken::create(
                    $user->id,
                    UserToken::TYPE_ACTIVATION,
                    Time::SECONDS_IN_A_DAY
                );
                Yii::$app->commandBus->handle(new SendEmailCommand([
                    'subject' => Yii::t('frontend', 'Activation email'),
                    'view' => 'activation',
                    'to' => $this->email,
                    'params' => [
                        'url' => Url::to(['/user/sign-in/activation', 'token' => $token->token], true)
                    ]
                ]));
            }
            return $user;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function shouldBeActivated()
    {
        /** @var Module $userModule */
        $userModule = Yii::$app->getModule('user');
        if (!$userModule) {
            return false;
        } elseif ($userModule->shouldBeActivated) {
            return true;
        } else {
            return false;
        }
    }
}
