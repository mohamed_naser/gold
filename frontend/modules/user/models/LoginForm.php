<?php
namespace frontend\modules\user\models;

use cheatsheet\Time;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $identity;
    public $password;
    public $rememberMe = true;

    private $user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['identity', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'identity'=>Yii::t('frontend', 'Username or email'),
            'password'=>Yii::t('frontend', 'Password'),
            'rememberMe'=>Yii::t('frontend', 'Remember Me'),
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', Yii::t('frontend', 'Incorrect username or password.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            if (Yii::$app->user->login($this->getUser(), $this->rememberMe ? Time::SECONDS_IN_A_MONTH : 0)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = User::find()
                ->active()
                ->andWhere(['or', ['username'=>$this->identity], ['email'=>$this->identity]])
                ->one();
        }

        return $this->user;
    }

    public function getUserProfile(){
    	$data = $this->getUser();

    	$profile['id']           = $data->id;
    	$profile['access_token'] = $data->access_token;
    	$profile['created_at']   = $data->created_at;
    	$profile['user_name']    = $data->username;
    	$profile['email']        = $data->email;
    	$profile['phone_number'] = $data->phone_number;
        $profile['longitude']    = $data->longitude;
        $profile['latitude']     = $data->latitude;
    	$profile['country_id']   = $data->country_id;
    	$profile['city_id']        = $data->city_id;
    	$profile['user_type']      = $data->user_type;
    	$profile['user_show_name'] = $data->user_show_name;
    	$profile['status']         = $data->status;
    	$profile['User_image']     = $data->user_image;
    	$profile['language']     = $data->language;

    	return $profile;
    }
}
