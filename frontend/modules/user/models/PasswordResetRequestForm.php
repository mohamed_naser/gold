<?php
namespace frontend\modules\user\models;

use cheatsheet\Time;
use common\commands\SendEmailCommand;
use common\models\UserToken;
use PHPMailer\PHPMailer\PHPMailer;
use Yii;
use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    /**
     * @var user email
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            $token = UserToken::create($user->id, UserToken::TYPE_PASSWORD_RESET, Time::SECONDS_IN_A_DAY);
            if ($user->save()) {
                return Yii::$app->commandBus->handle(new SendEmailCommand([
                    'to' => $this->email,
                    'subject' => Yii::t('frontend', 'Password reset for {name}', ['name'=>Yii::$app->name]),
                    'view' => 'passwordResetToken',
                    'params' => [
                        'user' => $user,
                        'token' => $token->token
                    ]
                ]));
            }
        }

        return false;
    }

    public function sendEmailToNormalUser(){

        /* @var $user User */
        $user = User::findOne([
            'email' => $this->email,
        ]);

        $token = UserToken::create($user->id, UserToken::TYPE_PASSWORD_RESET, Time::SECONDS_IN_A_DAY);
        if ($token) {
            $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/sign-in/reset-password?token='.$token]);

            $body = " يمكنك تغير رقمك السرى من خلال هذا الرابط $resetLink";
        }

        // true to get phpmailer exceptions
        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            // SMTP server example
            $mail->Host = "smtp.gmail.com";
            // enables SMTP debug information (for testing)
            $mail->SMTPDebug = 2;
            // enable SMTP authentication
            $mail->SMTPAuth = true;
            // set the SMTP port for the GMAIL server
            $mail->Port = 587;
            // SMTP account username example
            $mail->Username = 'm.naser.mailsender@gmail.com';
            // SMTP account password example
            $mail->Password = "9C4ws6!H";
            $mail->CharSet = 'UTF-8';

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom('from@gold.com', 'دهب');
            $mail->addAddress($user->email, $user->username);     // Add a recipient

            $mail->Subject = 'تفير كلمة المرور';
            $mail->Body    = $body;
            $mail->send();

        } catch (phpmailerException $e) {
            //Pretty error messages from PHPMailer
            echo $e->errorMessage();
            die();
        } catch (Exception $e) {
            //Boring error messages from anything else!
            echo $e->getMessage();
            die();
        }

        return TRUE;


    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'=>Yii::t('frontend', 'E-mail')
        ];
    }
}
