<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 09/07/17
 * Time: 01:33 م
 */
namespace frontend\modules\api\v1;

 use common\models\User;
 use yii\rest\ActiveController;

/**
 * This is the parent for all APIs deveoped.
 * All APIs will inherit from this class.
 *
 * The followings are the attributes used in all APIs:
 * @property array $output
 * @property array $result
 * @property array $error
 * @property integer $page
 * @property array $resonse
 *
 * @author code95-development-team (osama)
 */
class APIController extends ActiveController {

	/**
	 * Pre-defined statuses.
	 *
	 * @access protected
	 *
	 * @var    array    of error statuses.
	 */
	protected $_statuses;

	/**
	 * Pre-defined codes.
	 *
	 * @access protected
	 *
	 * @var    array    of error codes.
	 */
	protected $_statusesCodes;

	/**
	 * Pre-defined messages.
	 *
	 * @access protected
	 *
	 * @var    array    of error messages.
	 */
	protected $_messages;

	/**
	 * Pre-defined formats.
	 *
	 * @access protected
	 *
	 * @var    array    of output format.
	 */
	protected $_formats;

	/**
	 * Response output format.
	 *
	 * @access protected
	 *
	 * @var    string   response output format.
	 */
	protected $_format;

	/**
	 * Response status.
	 *
	 * @access protected
	 *
	 * @var    string       response status.
	 */
	protected $_status;

	/**
	 * Response code.
	 *
	 * @access protected
	 *
	 * @var    int        response code.
	 */
	protected $_statusCode;

	/**
	 * Response error message.
	 *
	 * @access protected
	 *
	 * @var    string       response message.
	 */
	protected $_message;

	/**
	 * Response data of the API.
	 *
	 * @access protected
	 *
	 * @var    string       response data.
	 */
	protected $_data = [];

	/**
	 * Operations constatns
	 */
	const STATUS_OKEY                   = 200;
	const STATUS_CREATED                = 201;
	const STATUS_ACCEPTED               = 202;
	const STATUS_NO_CONTENT             = 204;
	const STATUS_RESET_CONTENT          = 205;
	const STATUS_FOUND                  = 302;
	const STATUS_BAD_REQUEST            = 400;
	const STATUS_UNAUTHORIZED           = 401;
	const STATUS_FORBIDDEN              = 403;
	const STATUS_NOT_FOUND              = 404;
	const STATUS_METHOD_NOT_ALLOWED     = 405;
	const STATUS_NOT_ACCEPTABLE         = 406;
	const STATUS_CONFLICT               = 409;
	const STATUS_LENGTH_REQUIRED        = 411;
	const STATUS_PRECONDITION_FAILED    = 412;
	const STATUS_UNSUPPORTED_MEDIA_TYPE = 415;
	const STATUS_ERROR                  = 500;
	const STATUS_NOT_IMPLEMENTED        = 501;

	/**
	 * Output formats constants
	 */
	const JSON = 'json';
	const XML  = 'xml';
	const YML  = 'yml';

	/**
	 * @var array the default variable contains the output for all generated APIs
	 */
	public $output = [];

	/**
	 *
	 * @var array this variable will be hold all data retrived from any model
	 */
	public $result = [];

	/**
	 *
	 * @var array if the system generats errors, this variable contains the errors
	 */
	public $errors   = [];
	public $response = [];

	/**
	 * Used to check if the current requesting user is the same as the user that will have the operation applied on.
	 *
	 * @author  Mohamed Naser
	 *
	 * @param   int     $userId
	 *
	 * @return  void
	 * @TODO need to change functionality of this method
	 */
	protected function _getCurrentUser() {

		// if token not send as a GET parmater check it in headers
		if (!isset($_GET['Authorization'])) {
			$sentHeaders = $this->parseRequestHeaders();
			// check if token sent in headers
			if (key_exists('Authorization', $sentHeaders)) {
				$_GET['Authorization'] = $sentHeaders['Authorization'];
			}
		}

		if (!isset($_GET['Authorization'])) {
			return null;
		}

		$user = User::find()->where(['access_token' => $_GET['Authorization']])->one();
		if (!empty($user))
				return $user;

		return NULL;
	}

	public function _getCurentLanguage (){
        $sentHeaders = $this->parseRequestHeaders();

        if($sentHeaders['Accept-Language'] == 'en'){
            return 1;
        }else{
            return 0;
        }
    }

	private function _getStatusCodeMessage($status) {
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];

		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	public function _sendResponse($status = 200, $body = '', $content_type = 'application/json') {

		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		/* Set the response status code */
		header($status_header);

		/* Set the content type */
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');
		header('Content-type: ' . $content_type);

		$body = \GuzzleHttp\json_encode($body);

		//$this->log($status);
		if ($status == 200) {
			echo $body;
			exit;
		} else {
			$this->response['status'] = $status;
			$this->response['errors'] = $status;
			echo $body;
			exit;
		}
	}


	/**
	 * get the header authorization token value
	 * workaround for php that running over CGI not apache
	 * @author Ahmed Soliman <a.soliman@meunity.com>
	 *
	 * @return array
	 */
	function parseRequestHeaders() {
		if (function_exists("apache_request_headers")) {
			return apache_request_headers();
		}

		$headers = array();
		foreach ($_SERVER as $key => $value) {
			if (substr($key, 0, 5) <> 'HTTP_') {
				continue;
			}
			$header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
			$headers[$header] = $value;
		}
		return $headers;
	}


}
