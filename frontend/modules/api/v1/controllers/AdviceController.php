<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/07/17
 * Time: 05:45 م
 */


namespace frontend\modules\api\v1\controllers;

use common\models\City;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class AdviceController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Advice';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

    
}