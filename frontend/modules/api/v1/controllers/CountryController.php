<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 10/07/17
 * Time: 04:08 م
 */

namespace frontend\modules\api\v1\controllers;

use common\models\City;
use common\models\Country;
use frontend\modules\api\v1\APIController;


class CountryController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Country';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

    public function actionAll(){

        $countries = Country::find()->asArray()->all();

        if(empty($countries)){
            $this->_sendResponse(404 , ['result' => ' no Countries ']);
        }else{
            $this->_sendResponse(200 , ['result' => $countries ]);
        }
    }

	public function actionGetCountryCities(){
		$country_id = '';

		if(!empty($_POST['country_id']) && isset($_POST['country_id'])){
			$country_id = $_POST['country_id'];
		}else
			$this->_sendResponse(404 , ['result' => 'country_id is mandatory ']);

		$cities = Country::getCitiesByCountry($country_id);

		if(empty($cities)){
			$this->_sendResponse(404 , ['result' => ' no cities for this country ']);
		}else{
			$this->_sendResponse(200 , ['result' => $cities ]);
		}

	}


	public function actionGetAllCountriesWithCities(){
		$result = null;

		$data = Country::find()->where()->asArray()->all();
		foreach ($data as $row){
			$result_row['country_id']   = $row['id'];
			$result_row['country_name'] = $row['country_name'];
			$result_row['cities']       = Country::getCitiesByCountry($row['id']);

			$result[] = $result_row ;
		}

        if(empty($result)){
            $this->_sendResponse(404 , ['result' => ' no countries match this critira . ']);
        }else{
            $this->_sendResponse(200 , ['result' => $result ]);
        }

	}


}