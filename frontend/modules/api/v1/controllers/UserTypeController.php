<?php

namespace frontend\modules\api\v1\controllers;

use common\models\City;
use common\models\UserType;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class UserTypeController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\UserType';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

    public function actionListAll(){

        $usersTypes = UserType::find()->asArray()->all();

        if(empty($usersTypes)){
            $this->_sendResponse(404 , ['result' => ' no User type with this language . ']);
        }else{
            $this->_sendResponse(200 , ['result' => $usersTypes ]);
        }
    }

}