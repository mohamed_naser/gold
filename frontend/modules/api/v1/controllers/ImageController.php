<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 09/07/17
 * Time: 01:49 م
 */

namespace frontend\modules\api\v1\controllers;

 use common\models\Product;
 use common\models\ProductImages;
 use frontend\modules\api\v1\APIController;


class ImageController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Image';


	public function actionUpload (){
		$source     =  \Yii::getAlias('@uploadedimages');
		$image      = false;
		$time       = time();

		// check parameter [image]
		if(isset($_FILES['image']) && !empty($_FILES['image'])){
			$image = $_FILES['image'];
			$path  = $_FILES['image']['name'];
			$ext   = pathinfo($path, PATHINFO_EXTENSION);

		}else
			$this->_sendResponse(404 , ['result' => 'image is mandatory ']);

		$new_image_name = $time.'.'.$ext;
		$upload_status =  move_uploaded_file($image['tmp_name'] , $source.'/'.$new_image_name);

		if($upload_status){
			$this->_sendResponse(200 , ['result' => $new_image_name]);
		}else
			$this->_sendResponse(500 , ['result' => ' error in uploading image ']);

	}

}