<?php

namespace frontend\modules\api\v1\controllers;

use cheatsheet\Time;
use common\models\Product;
use common\models\User;
use common\models\UserToken;
use frontend\modules\api\v1\APIController;
use frontend\modules\api\v1\resources\User as UserResource;
use frontend\modules\user\models\LoginForm;
use frontend\modules\user\models\SignupForm;
use PHPMailer\PHPMailer\PHPMailer;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class UserController extends APIController
{
    /**
     * @var string
     */
    public $modelClass = 'frontend\modules\api\v1\resources\User';

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => AccessControl::className(),
            'rules' => [
	            [
		            'actions' => [
			             'user-register', 'user-login','get-user-products','view-profile','update-profile','list-shops','forget-password'
		            ],
		            'allow' => true,
		            'roles' => ['?']
	            ],
            ],
//            'authMethods' => [
//                [
//                    'class' => HttpBasicAuth::className(),
//                    'auth' => function ($username, $password) {
//                        $user = User::findByLogin($username);
//                        return $user->validatePassword($password)
//                            ? $user
//                            : null;
//                    }
//                ],
//                HttpBearerAuth::className(),
//                QueryParamAuth::className()
//            ]
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel']
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction'
            ]
        ];
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = UserResource::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    public function actionUserRegister(){
    	$data = $_POST ;

    	if(empty($data['user_name']) || !isset($data['user_name']))
		    $this->_sendResponse(404 , ['result' => 'user_name is mandatory field ']);
    	if(empty($data['email']) || !isset($data['email']))
		    $this->_sendResponse(404 , ['result' => 'email is mandatory field ']);
    	if(empty($data['password']) || !isset($data['password']))
		    $this->_sendResponse(404 , ['result' => 'user_name is mandatory field ']);


	    $model = new SignupForm();

	    $model->username  = $data['user_name'];
	    $model->email     = $data['email'];
	    $model->password  = $data['password'];
        $model->shop_name = $data['shop_name'];

	    if(!empty($data['country_id']) && isset($data['country_id']))
	    	$model->country_id = $data['country_id'];
	    if(!empty($data['city_id']) && isset($data['city_id']))
	    	$model->city_id = $data['city_id'];
	    if(!empty($data['phone_number']) && isset($data['phone_number']))
	    	$model->phone_number = $data['phone_number'];
	    if(!empty($data['longitude']) && isset($data['longitude']))
	    	$model->longitude = $data['longitude'];
	    if(!empty($data['latitude']) && isset($data['latitude']))
	    	$model->latitude = $data['latitude'];
	    if(!empty($data['user_type']) && isset($data['user_type']))
	    	$model->user_type = $data['user_type'];
	    if(!empty($data['user_show_name']) && isset($data['user_show_name']))
	    	$model->user_show_name = $data['user_show_name'];
	    if(!empty($data['user_image']) && isset($data['user_image']))
	    	$model->user_image = $data['user_image'];
	    if(!empty($data['language']) && isset($data['language']))
	    	$model->language = $data['language'];

        $user = $model->signup();
	    if ($user) {
		    if ($model->shouldBeActivated()) {
			    $this->_sendResponse(200 , ['result' => 'Your account has been successfully created. Check your email for further instructions.']);
		    }

	    }else{
		    $this->_sendResponse(500 , ['result' => $model->errors]);
	    }

	    $data['access_token'] = $user->access_token;
	    $this->_sendResponse(200 , ['result' => $data]);

    }

    public function actionUserLogin(){
	    $data = $_POST ;

	    if(empty($data['identity']) || !isset($data['identity']))
		    $this->_sendResponse(404 , ['result' => 'identity is mandatory field ']);
	    if(empty($data['password']) || !isset($data['password']))
		    $this->_sendResponse(404 , ['result' => 'user_name is mandatory field ']);

	    $model = new LoginForm();
	    $model->identity = $data['identity'];
	    $model->password = $data['password'];

	    if($model->login()){
		    $this->_sendResponse(200 , ['result' => $model->getUserProfile()]);
	    }else{
		    $this->_sendResponse(404 , ['result' => ' username or password in incorect ']);
	    }

    }

    public function actionGetUserProducts (){

	    $user_id = null;

	    $user = $this->_getCurrentUser();
	    if(!empty($user->id) && isset($user->id))
		    $user_id = $user->id;
	    else
		    $this->_sendResponse(404 , ['result' => 'undefined User']);

	    $products = Product::find()->where(['user_id' => $user_id])->asArray()->all();

	    if(!empty($products))
		    $this->_sendResponse(200 , ['result' => $products]);
	    else
		    $this->_sendResponse(200 , ['result' => 'no products for this user ']);

    }

    public function actionViewProfile($user_id){

    	$user = User::find()->where(['id' => $user_id])->select(['id as user_id' ,'email','user_image','status','longitude','latitude','language','user_type','country_id','city_id','username','phone_number','user_show_name'])->asArray()->one();

    	if(!empty($user)){
			$products = Product::find()->where(['user_id' => $user_id])->asArray()->limit(3)->all();

		    $user['products'] = $products;
	    }

	    $this->_sendResponse(200 , ['result' =>$user]);
    }

    public function actionUpdateProfile(){
    	$data = $_POST;

    	$user = $this->_getCurrentUser();

    	if(empty($user))
		    $this->_sendResponse(404 , ['result' =>'user not found']);

        if(isset($data['email']) && !empty($data['email']))
                    $user->email          = $data['email'];
        if(isset($data['user_name']) && !empty($data['user_name']))
                    $user->username        = $data['user_name'];
        if(isset($data['phone_number']) && !empty($data['phone_number']))
                    $user->phone_number   = $data['phone_number'];
    	if(isset($data['user_image']) && !empty($data['user_image']))
		    $user->user_image     = $data['user_image'];        
    	if(isset($data['longitude']) && !empty($data['longitude']))
		    $user->longitude            = $data['longitude'];
    	if(isset($data['latitude']) && !empty($data['latitude']))
		    $user->latitude            = $data['latitude'];
    	if(isset($data['user_type']) && !empty($data['user_type']))
		    $user->user_type      = $data['user_type'];
    	if(isset($data['language']) && !empty($data['language']))
		    $user->language      = $data['language'];
        if(isset($data['country_id']) && !empty($data['country_id'])){
                    $check_exststs = \common\models\Country::findOne($data['country_id']);
                    if(empty($check_exststs))
                                $this->_sendResponse( 404 , ['result' => 'country is not correct ']);
                    $user->country_id     = $data['country_id'];

        }
        if(isset($data['city_id']) && !empty($data['city_id'])){
                    $check_exststs = \common\models\City::findOne($data['city_id']);
                    if(empty($check_exststs))
                                $this->_sendResponse( 404 , ['result' => 'city is not correct ']);
                    $user->city_id        = $data['city_id'];
        }
    	if(isset($data['user_type']) && !empty($data['user_type']))
		    $user->user_type      = $data['user_type'];
    	if(isset($data['user_show_name']) && !empty($data['user_show_name']))
		    $user->user_show_name = $data['user_show_name'];
    	if(isset($data['shop_name']) && !empty($data['shop_name']))
		    $user->shop_name      = $data['shop_name'];

    	if($user->save())
                $this->_sendResponse(200 , ['result' =>'update profile success']);
	    else
                $this->_sendResponse(500 , ['result' => $user->errors]);
    }
    
    public function actionListShops(){
        $shops = User::find()->where(['user_type' => 2])->orWhere(['user_type' => 3])->asArray()->all();
    
        if(!empty($shops)){
            $this->_sendResponse(200 , ['result' => $shops]);
        }

        $this->_sendResponse(404 , ['result' =>'no shops fid ']);
    }

    public function actionForgetPassword($email)
    {

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $email,
        ]);

        if($user) {
            $token = UserToken::create($user->id, UserToken::TYPE_PASSWORD_RESET, Time::SECONDS_IN_A_DAY);
            if ($user->save()) {

                $resetLink = \Yii::$app->urlManager->createAbsoluteUrl(['/user/sign-in/reset-password?token='.$token]);

                $body = "يمكنك تغير رقمك السرى من خلال هذا الرابط $resetLink";
            }
        }else
            $this->_sendResponse(404, ['result' => 'mail is incorrect.']);


        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host = "smtp.gmail.com";    // SMTP server example
            $mail->SMTPDebug = 2;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->Port = 587;                    // set the SMTP port for the GMAIL server
            $mail->Username = 'm.naser.mailsender@gmail.com';            // SMTP account username example
            $mail->Password = "9C4ws6!H";            // SMTP account password example
            $mail->CharSet = 'UTF-8';

            //Recipients
            $mail->setFrom('from@gold.com', 'دهب');
            $mail->addAddress('m.naser@mitchdesigns.com', 'mohamed naser ');     // Add a recipient

            $mail->Subject = 'تفير كلمة المرور';
            $mail->Body    = $body;
            $mail->send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }

        if($mail->send())
            $this->_sendResponse(200, ['result' => 'mail send successfully.']);
        else
            $this->_sendResponse(500, ['result' => 'error ins ending mail.']);

    }
}
