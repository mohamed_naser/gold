<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 21/07/17
 * Time: 10:31 م
 */

namespace frontend\modules\api\v1\controllers;


use common\models\Brands;
use common\models\Manufacturer;
use common\models\ProductType;
use common\models\SpecialFor;
use frontend\modules\api\v1\APIController;


class ConstansController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Country';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];


	public function actionGetManfactors(){

		$manfactors = Manufacturer::find()->asArray()->all();

		if(empty($manfactors)){
			$this->_sendResponse(404 , ['result' => ' no manfactor exsits']);
		}else{
			$this->_sendResponse(200 , ['result' => $manfactors ]);
		}

	}

	public function actionGetSpecialFor(){

		$special_for = SpecialFor::find()->asArray()->all();

		if(empty($special_for)){
			$this->_sendResponse(404 , ['result' => ' no specials exsits']);
		}else{
			$this->_sendResponse(200 , ['result' => $special_for ]);
		}
	}

	public function actionGetProductType(){

		$product_types = ProductType::find()->asArray()->all();

		if(empty($product_types)){
			$this->_sendResponse(404 , ['result' => ' no product type exsits']);
		}else{
			$this->_sendResponse(200 , ['result' => $product_types ]);
		}
	}

	public function actionGetBrands(){

		$brands = Brands::find()->asArray()->all();

		if(empty($brands)){
			$this->_sendResponse(404 , ['result' => ' no brands exsits']);
		}else{
			$this->_sendResponse(200 , ['result' => $brands ]);
		}
	}


}