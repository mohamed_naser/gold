<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/07/17
 * Time: 06:58 م
 */


namespace frontend\modules\api\v1\controllers;

use common\models\City;
use common\models\Report;
use common\models\User;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class ReportController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Report';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

	public function verbs()
	{
		return [
			'index'  => ['GET', 'HEAD'],
			'view'   => ['GET'],
			'create' => ['POST'],
			'update' => ['PUT', 'PATCH'],
			'delete' => ['DELETE']
		];
	}

	public function actionSave(){
		$data = $_POST;

		$model = new Report();
		$model->title       = $data['title'];
		$model->description = $data['description'];
		$model->save();

		$this->_sendResponse(200 , ['result' => 'report added ' ]);

	}

}