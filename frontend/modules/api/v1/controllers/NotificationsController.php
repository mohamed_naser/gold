<?php

namespace frontend\modules\api\v1\controllers;

use common\models\FavoriteProduct;
use common\models\Notifications;
use frontend\modules\api\v1\APIController;

class NotificationsController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Product';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

	public function verbs()
	{
		return [
			'list-notifications'  => ['GET', 'HEAD'],
			'notification-counter'   => ['GET'],
			'create' => ['POST'],
			'update' => ['PUT', 'PATCH'],
			'delete' => ['DELETE']
		];
	}

	public function actionListNotifications(){

		$results = [];

		$user = $this->_getCurrentUser();

		if(empty($user))
			$this->_sendResponse(404 , ['result' =>  'undefinde User' ]);

		$notifications = Notifications::find()->where(['user_id' => $user->id])->all();

		if(!empty($notifications)){
			foreach ($notifications as $notification){
				$notification->read = 1;
				$notification->save();

				$row['notification_type']    = $notification->notification_type;
				$row['notification_message'] = $notification->notification_message;
				$row['read']                 = $notification->read;
				$row['created_at']           = $notification->created_at;

				$results[] = $row;
			}
		}

		if(empty($results)){
			$this->_sendResponse(404 , ['result' => 'no notifications for this user ']);
		}else{
			$this->_sendResponse(200 , ['result' =>  $results ]);
		}

	}

	public function actionNotificationsCounter (){
		$user = $this->_getCurrentUser();

		if(empty($user))
			$this->_sendResponse(404 , ['result' =>  'undefinde User' ]);

		$notifications_counter = Notifications::find()->where(['user_id' => $user->id , 'read' => 0])->count();

		$this->_sendResponse(200 , ['notifications_counter' =>  $notifications_counter ]);

	}

}
