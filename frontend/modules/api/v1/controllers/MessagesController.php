<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/07/17
 * Time: 06:58 م
 */


namespace frontend\modules\api\v1\controllers;

use common\models\Messages;
use common\models\Notifications;
use common\models\User;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class MessagesController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Message';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

	public function verbs()
	{
		return [
			'index'  => ['GET', 'HEAD'],
			'view'   => ['GET'],
			'create' => ['POST'],
			'update' => ['PUT', 'PATCH'],
			'delete' => ['DELETE']
		];
	}

	public function actionSend(){
		$data = $_POST;
		$user = $this->_getCurrentUser();

		if(empty($user) || !isset($user)){
			$this->_sendResponse(404 , ['result' => 'undefined user ' ]);
		}

		$chec_reciver_exsists = User::find()->where(['id' => $data['reciver_id']])->exists();

		if(!$chec_reciver_exsists)
			$this->_sendResponse(404 , ['result' => 'undefined user' ]);
		else{

			$model = new Messages();

			$model->sender_id  = $user->id;
			$model->reciver_id = $data['reciver_id'];
			$model->message    = $data['message'];
			$model->thread_id  = Messages::generateThreadID($user->id , $data['reciver_id']);
			$model->save();

			$notification = new Notifications();

			$notification->user_id              = $model->reciver_id;
			$notification->notification_type    = $notification::MESSAGE_NOTIFICATION_TYPE ;
			$notification->notification_message = " يوجد رسالة من $user->username " ;
			$notification->save();

			$this->_sendResponse(200 , ['thread_id' => $model->thread_id ]);
		}
	}

	public function actionGetThread (){

		$user = $this->_getCurrentUser();

		if(!empty($_POST['thread_id'])){

			$thread_id = $_POST['thread_id'];
			$messages = Messages::find()->where(['thread_id' => $thread_id])->orderBy(['created_at' => SORT_DESC])->asArray()->all();
		}

		$partner_profile = Messages::getPartnerProfile($user->id , $thread_id);

		$this->_sendResponse(200 , ['result' =>  ['partner_profile' => $partner_profile , 'messages' => $messages ] ]);

	}

	public function actionListThreads (){
		$user = $this->_getCurrentUser();

		$threads = Messages::find()->select(['thread_id'])
                                    ->where(['sender_id' => $user->id])
                                    ->orWhere(['reciver_id' => $user->id])
                                    ->orderBy(['created_at' => SORT_DESC])
                                    ->groupBy('thread_id')
                                    ->asArray()
                                    ->all();
		if(!empty($threads)){
			foreach ($threads as $thread_id ){
				$partner_profile = Messages::getPartnerProfile($user->id , $thread_id);
				$last_message    = Messages::find()->where(['thread_id' => $thread_id])->asArray()->one();
				$results[]       = ['partner_profile' => $partner_profile , 'last_message' => $last_message];

			}
		}else
			$this->_sendResponse(404 , ['result' => 'no threads' ]);

		$this->_sendResponse(200 , ['result' => $results]);


	}
}
