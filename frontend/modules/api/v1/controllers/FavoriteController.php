<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/07/17
 * Time: 03:16 م
 */

namespace frontend\modules\api\v1\controllers;

use common\models\FavoriteProduct;
use common\models\Product;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class FavoriteController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\FavoriteProduct';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

	public function actionSave(){

		$data       = $_POST;
		$user_id    = null;
		$product_id = null;

		$user = $this->_getCurrentUser();

		if(!empty($user->id) && isset($user->id))
			$user_id = $user->id;
		else
			$this->_sendResponse(404 , ['result' => 'undefined User']);

		if(!empty($data['product_id']) && isset($data['product_id']))
			$product_id = $data['product_id'];
		else
			$this->_sendResponse(404 , ['result' => 'undefined Product']);

		// check if user already favorite this product or not
		$check_favorite = FavoriteProduct::find()->where(['product_id' => $product_id , 'user_id' => $user_id])->exists();
		if(!$check_favorite){
			$mode = new FavoriteProduct();

			$mode->product_id = $product_id;
			$mode->user_id    = $user_id;

			if($mode->save())
				$this->_sendResponse(200 , ['result' => 'add to favorite success']);
			else
				$this->_sendResponse(200 , ['result' => 'error in saving data ']);
		}else
			$this->_sendResponse(200 , ['result' => ' product already added to favorite list ']);

	}

	public function actionDeleteFavorite (){
		$data       = $_POST;
		$user_id    = null;
		$product_id = null;

		$user = $this->_getCurrentUser();

		if(!empty($user->id) && isset($user->id))
			$user_id = $user->id;
		else
			$this->_sendResponse(404 , ['result' => 'undefined User']);

		if(!empty($data['product_id']) && isset($data['product_id']))
			$product_id = $data['product_id'];
		else
			$this->_sendResponse(404 , ['result' => 'undefined Product']);

		$delete = FavoriteProduct::deleteAll(['product_id' => $product_id , 'user_id' => $user_id]);

		if($delete)
			$this->_sendResponse(200 , ['result' => ' Delete successfully ']);
		else
			$this->_sendResponse(404 , ['result' => 'error in deletting ']);

	}

	public function actionListUserFavoriteProducts (){
		$user_id = null;

		$user = $this->_getCurrentUser();
		if(!empty($user->id) && isset($user->id))
			$user_id = $user->id;
		else
			$this->_sendResponse(404 , ['result' => 'undefined User']);

		$products = FavoriteProduct::find()->where(['user_id' => $user_id])->with('product')->asArray()->all();

		if(!empty($products))
			$this->_sendResponse(200 , ['result' => $products]);
		else
			$this->_sendResponse(200 , ['result' => 'no favorite products for this user ']);

	}

}