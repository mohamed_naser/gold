<?php

namespace frontend\modules\api\v1\controllers;

use common\models\City;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class CityController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\City';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];


	public function actionListAll(){

        $cities = City::find()->where(['language' => $this->_getCurentLanguage() ])->asArray()->all();

        if(empty($cities)){
            $this->_sendResponse(404 , ['result' => ' no Cities ']);
        }else{
            $this->_sendResponse(200 , ['result' => $cities ]);
        }
    }
}