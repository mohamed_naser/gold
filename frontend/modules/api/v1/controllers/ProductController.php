<?php

namespace frontend\modules\api\v1\controllers;

use common\models\FavoriteProduct;
use common\models\Messages;
use common\models\Notifications;
use common\models\Product;
use common\models\ProductImages;
use common\models\User;
use frontend\modules\api\v1\APIController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class ProductController extends APIController
{
	/**
	 * @var string
	 */
	public $modelClass = 'frontend\modules\api\v1\resources\Product';
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items'
	];

	public function verbs()
	{
		return [
			'index'  => ['GET', 'HEAD'],
			'view'   => ['GET'],
			'create' => ['POST'],
			'update' => ['Post'],
			'delete' => ['DELETE'],
			'delete-product' => ['DELETE'],
		];
	}

	public function actionGetProductImages(){
		// check parameter [product id ]
		if(isset($_POST['product_id']) && !empty($_POST['product_id']))
			$product_id = $_POST['product_id'];
		else
			$this->_sendResponse(404 , ['result' => 'product_id is mandatory ']);


		$data = ProductImages::find()->where(['product_id' => $product_id])->select(['source','image_name'])->asArray()->all();

		if(!empty($data))
			$this->_sendResponse(200 , ['result' => $data]);
		else
			$this->_sendResponse(404 , ['result' => 'no photos for this product']);

	}

	public function actionSave(){
		$data = $_POST;

		if(!empty($data['product_name']) && !isset($data['product_name']))
			$this->_sendResponse(404 , ['result' => 'product name is mandatory field ']);
		if(!empty($data['user_id']) && !isset($data['user_id']))
					$this->_sendResponse(404 , ['result' => 'user_id is mandatory field ']);

		$model = new Product();

		$model->product_name = $data['product_name'];
		$model->user_id      = $data['user_id'];

		if(!empty($data['country_id']) && isset($data['country_id']))
			$model->country_id = $data['country_id'];

		if(!empty($data['city']) && isset($data['city']))
			$model->city = $data['city'];

		if(!empty($data['user_type']) && isset($data['user_type']))
			$model->user_type = $data['user_type'];

		if(!empty($data['special_for']) && isset($data['special_for']))
			$model->special_for = $data['special_for'];

		if(!empty($data['price']) && isset($data['price']))
			$model->price = $data['price'];

		if(isset($data['price_type']))
            $model->price_type = $data['price_type'];

        if(isset($data['price_note']))
            $model->price_note = $data['price_note'];

		if(!empty($data['qty']) && isset($data['qty']))
			$model->qty = $data['qty'];

		if(!empty($data['Gold_type']) && isset($data['Gold_type']))
			$model->Gold_type = $data['Gold_type'];

		if(!empty($data['caliber']) && isset($data['caliber']))
			$model->caliber = $data['caliber'];

		if(!empty($data['weight']) && isset($data['weight']))
			$model->weight = $data['weight'];

		if(!empty($data['mobile']) && isset($data['mobile']))
			$model->mobile = $data['mobile'];

		if(!empty($data['notes']) && isset($data['notes']))
			$model->notes = $data['notes'];

		if(isset($data['product_type']))
			$model->product_type = $data['product_type'];

		if(!empty($data['manufacturer_id']) && isset($data['manufacturer_id']))
			$model->manufacturer_id = $data['manufacturer_id'];

		if(!empty($data['brand_id']) && isset($data['brand_id']))
			$model->brand_id = $data['brand_id'];

		//
		if(!empty($data['default_image_name']) && isset($data['default_image_name']))
			$model->default_image_name = $data['default_image_name'];

		if($model->save()){
			// save product images
			if(!empty($data['image'])){
				foreach ($data['image'] as $row ){
					// save  new record in database
					$imageModel= new ProductImages();

					$imageModel->product_id = $model->id;
					$imageModel->image_name = $row;
					$imageModel->source     = \Yii::getAlias('@uploadedimages');
					$result =  $imageModel->save();
				}

				if($result)
					$this->_sendResponse(200 , ['product_id' => $model->id]);
				else
					$this->_sendResponse(500 , ['result' => ' error in saving images ']);


				$notification = new Notifications();

				$notification->user_id              = $model->user_id;
				$notification->notification_type    = $notification::NEW_PRODUCT ;
				$notification->notification_message = " تم أضاة منتجك $model->product_name " ;
				$notification->save();

			}else
				$this->_sendResponse(200 , ['product_id' => $model->id]);

		}else
			$this->_sendResponse(500 , ['result' => $model->errors]);

 	}

    public function actionUpdateProduct(){
        $data = $_POST;

        if(empty($data['product_id']) || !isset($data['product_id']))
            $this->_sendResponse(404 , ['result' => 'product id is mandatory field ']);

        $model = Product::find()->where(['id' => $data['product_id']])->one();

        if(empty($model))
            return $this->_sendResponse(404 , ['result' => 'product not found .']);

        if(!empty($data['product_name']))
            $model->product_name = $data['product_name'];

        if(!empty($data['country_id']) && isset($data['country_id']))
            $model->country_id = $data['country_id'];

        if(!empty($data['city']) && isset($data['city']))
            $model->city = $data['city'];

        if(!empty($data['user_type']) && isset($data['user_type']))
            $model->user_type = $data['user_type'];

        if(!empty($data['special_for']) && isset($data['special_for']))
            $model->special_for = $data['special_for'];

        if(!empty($data['price']) && isset($data['price']))
            $model->price = $data['price'];

        if(isset($data['price_type']))
            $model->price_type = $data['price_type'];

        if(isset($data['price_note']))
            $model->price_note = $data['price_note'];

        if(!empty($data['qty']) && isset($data['qty']))
            $model->qty = $data['qty'];

        if(!empty($data['Gold_type']) && isset($data['Gold_type']))
            $model->Gold_type = $data['Gold_type'];

        if(!empty($data['caliber']) && isset($data['caliber']))
            $model->caliber = $data['caliber'];

        if(!empty($data['weight']) && isset($data['weight']))
            $model->weight = $data['weight'];

        if(!empty($data['mobile']) && isset($data['mobile']))
            $model->mobile = $data['mobile'];

        if(!empty($data['notes']) && isset($data['notes']))
            $model->notes = $data['notes'];

        if(isset($data['product_type']))
            $model->product_type = $data['product_type'];

        if(!empty($data['manufacturer_id']) && isset($data['manufacturer_id']))
            $model->manufacturer_id = $data['manufacturer_id'];

        if(!empty($data['brand_id']) && isset($data['brand_id']))
            $model->brand_id = $data['brand_id'];

        //
        if(!empty($data['default_image_name']) && isset($data['default_image_name']))
            $model->default_image_name = $data['default_image_name'];

        if($model->save()){
            // save product images
            if(!empty($data['image'])){
                foreach ($data['image'] as $row ){
                    // save  new record in database
                    $imageModel= new ProductImages();

                    $imageModel->product_id = $model->id;
                    $imageModel->image_name = $row;
                    $imageModel->source     = \Yii::getAlias('@uploadedimages');
                    $result =  $imageModel->save();
                }

                if($result)
                    $this->_sendResponse(200 , ['result' => 'product updated successfully']);
                else
                    $this->_sendResponse(500 , ['result' => ' error in saving images ']);


                $notification = new Notifications();

                $notification->user_id              = $model->user_id;
                $notification->notification_type    = $notification::NEW_PRODUCT ;
                $notification->notification_message = " تم أضاة منتجك $model->product_name " ;
                $notification->save();

            }else
                $this->_sendResponse(200 , ['result' => 'product updated successfully .']);

        }else
            $this->_sendResponse(500 , ['result' => $model->errors]);

    }

    public function actionShow(){
		$data = $_GET;
        $user = $this->_getCurrentUser();

		if(!empty($data['product_id']) && isset($data['product_id'])){

			$model = Product::find()->where(['id' => $data['product_id']])->with(['productImages'])->asArray()->one();

			if(!empty($model)){

			    if(!empty($user)) {
                    $model['is_favorite'] = FavoriteProduct::isfavorite($user->id, $data['product_id']);
                    $model['thread_id']   = Messages::generateThreadID($user->id,$model['user_id']);
                }else
                    $model['is_favorite'] = 0 ;

                    $this->_sendResponse(200 , ['result' => $model]);
			}else
				$this->_sendResponse(404 , ['result' => 'product not found']);

		}else
			$this->_sendResponse(404 , ['result' => 'product ID is mandatory field ']);
    }

    public function actionListProducts($page = 1 , $longitude = null , $latitude = null , $Gold_type = 1){

        $user = $this->_getCurrentUser();
        $result = null;
        $limit  = 10;

        $offset = ($page - 1) * $limit;
	    $products = Product::find()->where(['activate' => 1 , 'Gold_type' => $Gold_type])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->asArray()
                                    ->offset($offset)
                                    ->limit($limit)
                                    ->all();

            if(!empty($products)){
                    foreach ($products as $product){
                            $product_owner = User::find($product['user_id'])->asArray()->one();
                            $product_latitude  = $product_owner['latitude'];
                            $product_longitude = $product_owner['longitude'];

                            if(empty($longitude) || empty($latitude) ||empty($product_owner) || $product_owner['longitude'] == null || $product_owner['latitude'] == null)
                                $distance = -1 ;
                            else{

                                $distance = $this->get_distance($latitude , $longitude , $product_latitude , $product_longitude);
                            }

                            $product['distance'] = $distance;
                            if(!empty($user))
                                    $product['is_favorite'] = FavoriteProduct::isfavorite($user->id , $product['id']);
                            else
                                    $product['is_favorite'] = 0 ;

                            $result[] = $product;
                    }
            }else
	            $this->_sendResponse(404 , ['result' => 'there is no product to list . ']);

	    if(!empty($result))
		    $this->_sendResponse(200 , ['result' => $result]);
		else
			$this->_sendResponse(404 , ['result' => 'there is no product to list . ']);

    }

    public function actionListPaidProducts($page = 1 , $longitude = null , $latitude = null , $Gold_type = 1){

        $user = $this->_getCurrentUser();

        $result = null;
        $limit = 10 ;

        $offset = ($page - 1) * $limit;

        $products = Product::find()
                                ->where(['activate' => 1 , 'is_paid_adds' => Product::$paid_ads , 'Gold_type' => $Gold_type])
                                ->orderBy(['id' => SORT_DESC])
                                ->asArray()
                                ->offset($offset)
                                ->limit($limit)
                                ->all();

        if(!empty($products)){
            foreach ($products as $product){
                $product_owner = User::find($product['user_id'])->asArray()->one();
                $product_latitude  = $product_owner['latitude'];
                $product_longitude = $product_owner['longitude'];

                if(empty($longitude) || empty($latitude) ||empty($product_owner) || $product_owner['longitude'] == null || $product_owner['latitude'] == null)
                    $distance = -1 ;
                else{

                    $distance = $this->get_distance($latitude , $longitude , $product_latitude , $product_longitude);
                }

                $product['distance'] = $distance;

                if(!empty($user))
                        $product['is_favorite'] = FavoriteProduct::isfavorite($user->id , $product['id']);
                else
                        $product['is_favorite'] = 0 ;

                $result[] = $product;
            }
        }else
            $this->_sendResponse(404 , ['result' => 'there is no product to list . ']);

        if(!empty($result))
                $this->_sendResponse(200 , ['result' => $result]);
            else
                $this->_sendResponse(404 , ['result' => 'there is no product to list . ']);

    }

    public function actionDeleteProduct($product_id){

        $product = Product::find()->where(['id'=>$product_id])->one();

        if(empty($product))
            $this->_sendResponse(404 , ['result' => 'there is no product withi this id .']);
        else{
            if($product->delete())
                $this->_sendResponse(404 , ['result' => 'product deleted success . ']);
            else
                $this->_sendResponse(200 , ['result' => 'Error in deleteding product  . ']);
        }

    }
    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function get_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo   = deg2rad($latitudeTo);
        $lonTo   = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }
    public function actionSearch (){
    	$data = $_GET;
	    $conditions = array();
	    $user     = $this->_getCurrentUser();

	    $products = Product::find();
        $previous_week_time = strtotime('-1 week');
        $previous_month_time = strtotime('-1 month');
        $previous_week = date('Y-m-d',$previous_week_time);
        $previous_month = date('Y-m-d',$previous_month_time);

		if(isset($data['user_id']) && !empty($data['user_id'])){
			$conditions['user_id'] =  $data['user_id'];
		}
		if(isset($data['limit']))
            $limit = $data['limit'];
		else
		    $limit = 20;
		if(isset($data['offest']))
            $offest = $data['offest'];
		else
            $offest = 0;

		if(isset($data['country_id']) && !empty($data['country_id'])){
			$conditions['country_id'] = $data['country_id'];
		}
		if(isset($data['city']) && !empty($data['city'])){
			$conditions['city'] = $data['city'];
		}
		if(isset($data['user_type']) && !empty($data['user_type'])){
			$conditions['user_type'] = $data['user_type'];
		}
		if(isset($data['special_for']) && !empty($data['special_for'])){
			$conditions['special_for'] = $data['special_for'];
		}
		if(isset($data['qty']) && !empty($data['qty'])){
			$conditions['qty'] = $data['qty'];
		}
		if(isset($data['caliber']) && !empty($data['caliber'])){
			$conditions['caliber'] = $data['caliber'];
		}
		if(isset($data['special_for']) && !empty($data['special_for'])){
			$conditions['special_for'] = $data['special_for'];
		}
		if(isset($data['manufacturer_id']) && !empty($data['manufacturer_id'])){
			$conditions['manufacturer_id'] = $data['manufacturer_id'];
		}
		if(isset($data['brand_id']) && !empty($data['brand_id'])){
			$conditions['brand_id'] = $data['brand_id'];
		}
		if(isset($data['Gold_type']) && !empty($data['Gold_type'])){
			$conditions['Gold_type'] = $data['Gold_type'];
		}
		if(isset($data['product_type'])){
			$conditions['product_type'] = $data['product_type'];
		}
			$conditions['activate'] = 1;

        if(isset($data['time_limit'])){
            switch ($data['time_limit']){
                // today search
                case 0:
                    $conditions['date(created_at)'] =  date('Y-m-d');
                        break;
                // search for week
                case 1:
                    $products->andwhere(['>=' , 'date(created_at)', $previous_week]);
                    break;
                // search for month
                case 2:
                    $products->andwhere(['>=' , 'date(created_at)', $previous_month]);
                    break;
            }
        }

		$products = $products->where($conditions);

		// conditons have rang of value
		if(isset($data['price_from']) && !empty($data['price_from'])){
			$products->andWhere(['>=' , 'price' , $data['price_from'] ]);
		}
	    if(isset($data['weight_from']) && !empty($data['weight_from']) ){
		    $products->andWhere(['>=' , 'weight' , $data['weight_from'] ]);
	    }

		// conditions with like operator
	    if(isset($data['product_name'])){
			$products->andFilterWhere(['like' , 'product_name' , $data['product_name']]);
	    }
	    $products = $products->orderBy('created_at desc')->limit($limit)->offset($offest)->asArray()->with('productImages');
	    $products = $products->all();

	    if(isset($data['with_images']) && $data['with_images'] == 1){
		    foreach ($products as $product){
		    	if(count($product['productImages']) > 0)
						$results [] = $product;
		    }
	    }else
		    $results = $products;

		if(!empty($results)){
			$final_results =[];
			foreach ($results as $product){

				if(!empty($user->id) && isset($user->id)){
					$product['is_favorite'] = FavoriteProduct::isfavorite($user->id , $product['id']);
				}else
					$product['is_favorite'] = 0 ;

				$final_results [] = $product ;
			}
			$this->_sendResponse(200 , ['result' => $final_results ]);
		}else
			$this->_sendResponse(200 , ['result' => 'there is no product match this critira . ']);

    }

    // $price range examples  "500:600" or "100:300"
	// to sorting by hotel name value of $sort will be 'Name' and for sort by price $sort = 'Price'

    public function actionFox($pric_range = null , $destination =null , $hotelName = null , $date_range =null , $sort = null){
    	// testing values
	    $pric_range  = "100:105";
//	    $date_range  = "8-10-2020:14-10-2020";
//		$destination = 'Par';
//	    $hotelName   = 'Media';

	    // filteration test
//	    $sort = 'Name';
	    $sort = 'Price';

	    $data = file_get_contents("https://api.myjson.com/bins/tl0bp");
	    $data = \GuzzleHttp\json_decode($data , true);

	    $beta_hotels = $data['hotels'];

	    // filter results according to conditions
	    if(!is_null($pric_range)){
		    $prices = explode(":", $pric_range);
		    $beta_hotels =  $this->filterByPrice($prices[0] , $prices[1] , $beta_hotels);
	    }

	    if(!is_null($date_range)){
		    $dates = explode(":", $date_range);
		    $beta_hotels =  $this->filterByDate($dates[0] , $dates[1] , $beta_hotels);
	    }

	    if(!is_null($destination)){
		    $beta_hotels =  $this->filterByDestination($destination , $beta_hotels);
	    }

	    if(!is_null($hotelName)){
		    $beta_hotels =  $this->filterByHotelName($hotelName , $beta_hotels);
	    }

	    // result sorting
	    if(!empty($beta_hotels) && !is_null($sort)){

			if($sort == 'Name'){
				array_multisort(
					$beta_hotels->name,
					$beta_hotels->price, SORT_ASC, SORT_NUMERIC,
					$beta_hotels['city'],
					$beta_hotels['availability']
				);
			}elseif ($sort = 'Price'){
				$hotels_sorted_by_prices  = [];
				foreach ($beta_hotels as $one){
					$hotels_sorted_by_prices[$one['price']] = $one;
				}
				$beta_hotels = $hotels_sorted_by_prices;
				ksort($beta_hotels);
			}
	    }

	    print_r($beta_hotels);die();

    }

    public function filterByPrice ($pric_from , $to_price  , $hotels){
		$results =[];

    	foreach ($hotels as $hotel){
    		if($hotel['price'] >= $pric_from && $hotel['price'] <= $to_price){
    			$results [] = $hotel;
		    }
	    }

	    return $results;
    }

	public function filterByDate ($date_from , $to_date  , $hotels){
		$results =[];
		$date_from = strtotime($date_from);
		$to_date   = strtotime($to_date);

		foreach ($hotels as $hotel){
			$available_dayes = $hotel['availability'];

			foreach ($available_dayes as $row){

				if(strtotime($row['from']) <= $date_from && strtotime ($row['to']) >= $to_date){
					$results [] = $hotel;
				}
			}
		}

		return $results;
	}

    public function filterByDestination ($destination , $hotels){

    	$results = [];

    	foreach ($hotels as $hotel ){
    		if( is_numeric(stripos($hotel['city'] , $destination))){
				$results [] = $hotel;
		    }
	    }

	    return $results;
    }

	public function filterByHotelName ($hotelName , $hotels){

		$results = [];

		foreach ($hotels as $hotel ){
			if( is_numeric(stripos($hotel['name'] , $hotelName))){
				$results [] = $hotel;
			}
		}

		return $results;
	}
}
