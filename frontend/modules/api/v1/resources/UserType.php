<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 07/07/17
 * Time: 11:16 م
 */

namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class UserType extends \common\models\UserType implements Linkable
{
	public function fields()
	{
		return [ 'id',  'user_type'];
	}

//	public function extraFields()
//	{
//		return ['category'];
//	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['user-type/view', 'id' => $this->id], true)
		];
	}
}