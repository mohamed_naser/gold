<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/07/17
 * Time: 05:46 م
 */

namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Advice extends \common\models\GeneralAdvice implements Linkable
{
	public function fields()
	{
		return ['advice_id', 'title', 'description' ];
	}

	public function extraFields()
	{
		return ['category'];
	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['article/view', 'advice_id' => $this->advice_id], true)
		];
	}
}
