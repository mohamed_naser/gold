<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 09/07/17
 * Time: 02:19 م
 */

namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Image extends \common\models\ProductImages implements Linkable
{
	public function fields()
	{
		return [ 'id' , 'source' , 'image_name' ,'product_id' ];
	}


	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['article/view', 'id' => $this->id], true)
		];
	}
}