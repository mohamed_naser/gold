<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/07/17
 * Time: 07:00 م
 */


namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Report extends \common\models\Report implements Linkable
{
	public function fields()
	{
		return [
			'report_id'   ,
			'title'       ,
			'description' ,
			'created_at'  ,
			'reported_by' ,
			'reported_to' ,

			];
	}


	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['article/view', 'report_id' => $this->report_id], true)
		];
	}
}