<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 10/07/17
 * Time: 04:09 م
 */

namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class FavoriteProduct extends \common\models\FavoriteProduct
{
	public function fields()
	{
		return [   'created_at', 'user_id' ,  'product_id'];
	}

//	public function extraFields()
//	{
//		return ['category'];
//	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */

}
