<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 10/07/17
 * Time: 04:09 م
 */

namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Country extends \common\models\Country implements Linkable
{
	public function fields()
	{
		return [   'id', 'country_name' ,  'created_at'];
	}

//	public function extraFields()
//	{
//		return ['category'];
//	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['city/view', 'id' => $this->id], true)
		];
	}
}
