<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 07/07/17
 * Time: 04:03 م
 */


namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Product extends \common\models\Product implements Linkable
{
	public function fields()
	{
		return [
					'id', 'special_for', 'product_name','price','city','qty', 'caliber','weight','user_type', 'mobile','notes','created_at'
					,'user_id','default_image_source', 'default_image_name'
				];
	}

	public function extraFields()
	{
		return ['category'];
	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['product/view', 'id' => $this->id], true)
		];
	}
}
