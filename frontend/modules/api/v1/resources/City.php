<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 07/07/17
 * Time: 11:13 م
 */


namespace frontend\modules\api\v1\resources;

use yii\helpers\Url;
use yii\web\Linkable;
use yii\web\Link;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class City extends \common\models\City implements Linkable
{
	public function fields()
	{
		return [ 'id',  'city_name' , 'created_at', ];
	}

//	public function extraFields()
//	{
//		return ['category'];
//	}

	/**
	 * Returns a list of links.
	 *
	 * @return array the links
	 */
	public function getLinks()
	{
		return [
			Link::REL_SELF => Url::to(['city/view', 'id' => $this->id], true)
		];
	}
}
